﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat {
    public class OrbitalCameraController : CameraController
    {
        private Transform charTransform;

        private float currentHorizontalAngle = 0f;
        private float currentVerticalAngle = 0f;

        private float targetHorizontalAngle = 0f;
        private float targetVerticalAngle = 0f;

        private float currentZoom = 0f;
        private float targetZoom = 0f;

        private Vector3 currentTranslation = Vector3.zero;
        private Vector3 targetTranslation = Vector3.zero;

        private bool tooNear = false;
        private Vector3 lastForward = Vector3.zero;
        private bool lastNear = false;
        private float lastYAngle = 0f;

        bool printStatus = false;

        public OrbitalCameraController(Transform cam, Transform character, CharacterCameraData data) : base(cam, data)
        {
            charTransform = character;
        }
        
        public override void Tick(CharacterInputData inputData)
        {
            if (inputData.actionPressed)
            {
                printStatus = true;
            } else
            {
                printStatus = false;
            }

            ResetCamToCharacterPosition();
            SetBaseOffset();
            CalculateInputRotation(inputData);
            SmoothZoomAndRotations();
            ApplyTranslation();
            ApplyRotation();
            ManageCollisions();
            ManageCloseToPlayer();
            ApplyZoom();
        }

        private void ApplyZoom()
        {
            camTransform.position = camTransform.position - camTransform.forward * currentZoom;
        }

        private void ApplyRotation()
        {
            camTransform.rotation = Quaternion.Euler(currentVerticalAngle, currentHorizontalAngle, 0f);
        }

        private void ApplyTranslation()
        {
            currentTranslation = Vector3.MoveTowards(currentTranslation, targetTranslation, camData.TranslationSpeed * Time.deltaTime);
            camTransform.Translate(currentTranslation);
        }

        private void SmoothZoomAndRotations()
        {
            targetVerticalAngle = Mathf.Clamp(targetVerticalAngle, camData.MinAngleY, camData.MaxAngleY);
            currentHorizontalAngle = Mathf.SmoothStep(currentHorizontalAngle, targetHorizontalAngle, camData.SmoothSpeedX * Time.deltaTime);
            currentVerticalAngle = Mathf.SmoothStep(currentVerticalAngle, targetVerticalAngle, camData.SmoothSpeedY * Time.deltaTime);
            currentZoom = Mathf.SmoothStep(currentZoom, targetZoom, camData.SmoothZoom * Time.deltaTime);
        }

        private void ManageCloseToPlayer()
        {
            if (targetZoom <= camData.MinZoomToPlayerCollision || tooNear)
            {
                targetVerticalAngle += camData.PlayerCollisionYAngleSpeed * Time.deltaTime;
            }
        }

        private void PrintStatus(string text)
        {
            if (printStatus == true)
            {
                Debug.Log(text);
            }
        }

        // Camera steps for the raycasts checks. First check like the zoom raycast, and if it doesn't collides, all good.
        // Then, for every step beggining for the last one, check raycast on right, -right, up and -up. If any of them collides, get the zoom one step less and check again.
        // For the last step (the nearest to the player), if it collides, do the Y rotation thing.
        // TODO: Collisions are checked BUT we must trigger a different behaviour depending on wich raycast failed. For example, if all the Right raycasts failed but the depth raycasts all succeded,
        // it must mean that the player is near a wall. If failed the right and left ones, the player must be in a narrow place. If only failed the depth ones, the camera is near a wall (the easy case, already done).
        private void ManageCollisions()
        {
            tooNear = false;
            targetZoom = camData.Zoom;
            Ray ray = new Ray(camTransform.position, -camTransform.forward);
            RaycastHit hitInfo;
            float stepDistance = GetStepDistance();
            PrintStatus("Checking collisions");
            if (Physics.Raycast(ray, out hitInfo, targetZoom, camData.RaycastMask) || !CheckStep(stepDistance, camData.RaycastSteps, targetZoom))
            {
                PrintStatus("Collided on raycast or last (" + camData.RaycastSteps + ") step");
                PrintStatus("Hitinfo distance is: " + hitInfo.distance);
                Debug.DrawLine(camTransform.position, camTransform.position - camTransform.forward * camData.Zoom, Color.red);
                float minDistance = hitInfo.distance > 0f ? hitInfo.distance : camData.Zoom;
                for (int i = camData.RaycastSteps - 1; i >= 0; --i)
                {
                    if (i == 0)
                    {
                        targetZoom = stepDistance;
                        tooNear = true;
                        lastNear = true;
                        lastYAngle = targetVerticalAngle;
                        //lastForward = camTransform.forward;
                        break;
                    }
                    bool validStep = CheckStep(stepDistance, i, minDistance);
                    if (validStep == true)
                    {
                        PrintStatus("Valid step: " + i);
                        break;
                    }
                }
            }
            else
            {
                Debug.DrawLine(camTransform.position, camTransform.position - camTransform.forward * camData.Zoom, Color.blue);
                if (lastNear)
                {
                    lastForward = camTransform.forward;
                    lastForward.y = camData.SeparationTestAngle;
                    lastForward.Normalize();

                    Debug.DrawLine(camTransform.position, camTransform.position - lastForward * camData.Zoom, Color.green);
                    Ray ray2 = new Ray(camTransform.position, -lastForward);
                    RaycastHit hitInfo2;
                    if (!Physics.Raycast(ray2, out hitInfo2, camData.Zoom, camData.RaycastMask))
                    {
                        lastNear = false;
                        targetVerticalAngle = camData.SeparationTestReturnAngle;
                    }
                }
            }
        }

        private bool CheckStep(float stepDistance, int i, float minDistance)
        {
            if (minDistance < stepDistance*i)
            {
                PrintStatus("Step " + i + " not valid by distance");
                return false;
            }
            bool validStep = true;
            for (int j = 0; j < 4; ++j)
            {
                validStep = CheckStepRaycasts(stepDistance, i, j);
                if (validStep == false)
                {
                    break;
                }
            }
            if (validStep == true)
            {
                targetZoom = i * stepDistance;
            }
            return validStep;
        }

        private bool CheckStepRaycasts(float stepDistance, int i, int j)
        {
            Vector3 direction = Vector3.zero;
            Vector3 position = camTransform.position - camTransform.forward * stepDistance * i;
            switch (j)
            {
                case 0: // Up
                    direction = camTransform.up;
                    break;
                case 1: // Down
                    direction = -camTransform.up;
                    break;
                case 2: // Right
                    direction = camTransform.right;
                    break;
                case 3: // Left
                    direction = -camTransform.right;
                    break;
                default:
                    break;
            }
            
            Ray stepRay = new Ray(position, direction.normalized);
            bool valid = true;
            if (Physics.Raycast(stepRay, camData.RaycastRadius, camData.RaycastMask))
            {
                PrintStatus("Step " + i.ToString() + " not valid on case " + j.ToString());
                valid = false;
                Debug.DrawLine(position, position + direction * camData.RaycastRadius, Color.red);
            } else
            {
                Debug.DrawLine(position, position + direction * camData.RaycastRadius, Color.blue);
            }

            return valid;
        }

        private float GetStepDistance()
        {
            float stepDistance;
            if (camData.RaycastSteps <= 1)
            {
                stepDistance = camData.Zoom;
            }
            else
            {
                stepDistance = camData.Zoom / camData.RaycastSteps;
            }

            return stepDistance;
        }

        private void CalculateInputRotation(CharacterInputData inputData)
        {
            float verticalDelta = inputData.verticalLook * camData.MovementSpeedY * Time.deltaTime;
            float horizontalDelta = inputData.horizontalLook * camData.MovementSpeedX * Time.deltaTime;
            targetHorizontalAngle += horizontalDelta;
            targetVerticalAngle += verticalDelta;
        }

        private void SetBaseOffset()
        {
            targetTranslation.x = camData.OffsetX;
            targetTranslation.y = camData.OffsetY;
        }

        private void ResetCamToCharacterPosition()
        {
            camTransform.position = charTransform.position;
        }
    }
}