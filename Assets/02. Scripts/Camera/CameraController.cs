﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    public abstract class CameraController
    {
        protected Transform camTransform;
        protected CharacterCameraData camData;


        public CameraController(Transform cam, CharacterCameraData data)
        {
            camTransform = cam;
            camData = data;
        }

        public abstract void Tick(CharacterInputData inputData);
    }
}