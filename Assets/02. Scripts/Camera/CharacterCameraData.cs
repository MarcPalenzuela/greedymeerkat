﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    [CreateAssetMenu(menuName = "Cameras/Camera Data")]
    public class CharacterCameraData : ScriptableObject
    {
        [Header("Zoom Settings")]
        [SerializeField]
        private float zoom;
        public float Zoom { get { return zoom; } }

        [SerializeField]
        private float smoothZoom;
        public float SmoothZoom { get { return smoothZoom; } }

        [Header("Basic offsets")]
        [SerializeField]
        private float offsetY;
        public float OffsetY { get { return offsetY; } }

        [SerializeField]
        private float offsetX;
        public float OffsetX { get { return offsetX; } }

        [Header("Rotation/Movement speed")]
        [SerializeField]
        private float movementSpeedX;
        public float MovementSpeedX { get { return movementSpeedX; } }

        [SerializeField]
        private float smoothSpeedX;
        public float SmoothSpeedX { get { return smoothSpeedX; } }

        [SerializeField]
        private float movementSpeedY;
        public float MovementSpeedY { get { return movementSpeedY; } }

        [SerializeField]
        private float smoothSpeedY;
        public float SmoothSpeedY { get { return smoothSpeedY; } }

        [SerializeField]
        private float translationSpeed;
        public float TranslationSpeed { get { return translationSpeed; } }

        [Header("Y Angle limits")]
        [SerializeField]
        private float minAngleY;
        public float MinAngleY { get { return minAngleY; } }

        [SerializeField]
        private float maxAngleY;
        public float MaxAngleY { get { return maxAngleY; } }

        [Header("Collision parameters")]
        [SerializeField]
        private LayerMask raycastMask;
        public LayerMask RaycastMask { get { return raycastMask; } }

        [SerializeField]
        private float collisionZoomOffset;
        public float CollisionZoomOffset { get { return collisionZoomOffset; } }

        [SerializeField]
        private int raycastSteps;
        public int RaycastSteps { get { return raycastSteps; } }
        
        [SerializeField]
        private float raycastRadius;
        public float RaycastRadius { get { return raycastRadius; } }


        [Header("Near player parameters")]
        [SerializeField]
        private float minZoomToPlayerCollision;
        public float MinZoomToPlayerCollision { get { return minZoomToPlayerCollision; } }

        [SerializeField]
        private float playerCollisionYAngleSpeed;
        public float PlayerCollisionYAngleSpeed { get { return playerCollisionYAngleSpeed; } }

        [SerializeField]
        private float separationTestAngle;
        public float SeparationTestAngle { get { return separationTestAngle; } }

        [SerializeField]
        private float separationTestReturnAngle;
        public float SeparationTestReturnAngle { get { return separationTestReturnAngle; } }
    }
}