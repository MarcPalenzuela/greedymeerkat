﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    public class CharacterCamera : MonoBehaviour
    {
        [SerializeField]
        private CharacterCameraData cameraData;
        [SerializeField]
        private Camera cam;
        [SerializeField]
        private CharacterCameraSet cameraSet;

        private ICharacterInput input;
        private CameraController controller;

        private void Awake()
        {
            if (cam == null)
            {
                cam = Camera.main;
            }
            input = new PlayerInput();
        }

        public void Update()
        {
            if (controller != null)
            {
                CharacterInputData cid = input.Tick();
                controller.Tick(cid);
            }
        }

        public void FollowCharacter(Transform charTransform)
        {
            controller = new OrbitalCameraController(cam.transform, charTransform, cameraData);
        }

        private void OnEnable()
        {
            cameraSet.Add(this);
        }

        private void OnDisable()
        {
            cameraSet.Remove(this);
        }
    }
}