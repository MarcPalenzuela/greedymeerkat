﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovement : MonoBehaviour
{
    public float speed = 5f;
    Transform cam;

    private void Start()
    {
        cam = Camera.main.transform;
    }

    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float vertical = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        Vector3 forward = cam.forward;
        forward.y = 0f;
        Vector3 right = cam.right;
        right.y = 0f;

        //transform.Translate(new Vector3(horizontal, 0f, vertical));
        transform.Translate(forward * vertical + right * horizontal);
    }
}
