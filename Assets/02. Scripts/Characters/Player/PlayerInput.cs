﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    public class PlayerInput : ICharacterInput
    {
        private CharacterData data;
        private Character character;
        private CharacterInputData inputData;

        //Añadir input vertical y horizontal

        public void Init(CharacterData d, Character c)
        {
            data = d;
            character = c;
        }

        public CharacterInputData Tick()
        {
            CharacterInputData cid = new CharacterInputData();
            if (Input.GetButtonDown("Fire1"))
            {
                cid.actionPressed = true;
            }
            cid.inputHorizontal = Input.GetAxis("Horizontal");
            cid.inputVertical = Input.GetAxis("Vertical");
            cid.horizontalLook = Input.GetAxis("LookX");
            cid.verticalLook = Input.GetAxis("LookY");

            if (Input.GetButtonDown("Jump")) {
                cid.jumpAction = true;
            }

            return cid;
        }
    }
}