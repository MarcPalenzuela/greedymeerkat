﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    public abstract class CharacterController
    {
        protected ScriptableState state;
        public Character character;
        public CharacterData data;
        private UnityEngine.CharacterController cc;

        [SerializeField]
        private float curVerticalSpeed;
        public float CurVerticalSpeed { get => curVerticalSpeed; set => curVerticalSpeed = value; }

        [SerializeField]
        private bool madeSecondJump = false;
        public bool MadeSecondJump { get => madeSecondJump; set => madeSecondJump = value; }

        public void Init(CharacterData d, Character c)
        {
            character = c;
            data = d;
            state = data.StartingState;
            cc = c.GetComponent<UnityEngine.CharacterController>();
        }

        public void ChangeState(ScriptableState newState)
        {
            state.OnExitState(this);
            newState.OnEnterState(this);
            state = newState;
        }

        public void Tick(CharacterInputData inputData)
        {
            state.Tick(this, inputData);
            cc.Move(Vector3.down * 9.8f * Time.deltaTime);
            OnTick(inputData);
        }

        public abstract void OnTick(CharacterInputData inputData);
    }
}