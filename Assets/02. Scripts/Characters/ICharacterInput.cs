﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    public struct CharacterInputData
    {
        public bool actionPressed;
        public float inputHorizontal;
        public float inputVertical;
        public float horizontalLook;
        public float verticalLook;
        public bool jumpAction;
    }

    public interface ICharacterInput
    {
        void Init(CharacterData d, Character c);
        CharacterInputData Tick();
    }
}