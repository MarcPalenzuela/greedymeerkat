﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    [CreateAssetMenu]
    public class CharacterData : ScriptableObject
    {
        [SerializeField]
        private string type;
        public string Type { get { return type; } }

        [SerializeField]
        private ScriptableState startingState;
        public ScriptableState StartingState { get { return startingState; } }

        [SerializeField]
        private List<CharacterSet> setsToAdd;
        public List<CharacterSet> SetsToAdd { get { return setsToAdd; } }

        [SerializeField]
        private int hp;
        public int HP { get { return hp; } }
        [SerializeField]
        private float walkSpeed;
        public float Walkspeed { get { return walkSpeed; } }
        [SerializeField]
        private float jumpSpeed;
        public float JumpSpeed { get { return jumpSpeed; } }
        [SerializeField]
        private float fallSpeed;
        public float FallSpeed { get { return fallSpeed; } }
    }
}