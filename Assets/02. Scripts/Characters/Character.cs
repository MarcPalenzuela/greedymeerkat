﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    public class Character : MonoBehaviour
    {
        [SerializeField]
        private CharacterData data;
        private CharacterController controller;
        private ICharacterInput input;

        [SerializeField]
        private CharacterCameraSet cameraSet;

        private void Start()
        {
            // Depending on what kind of character generate one type of input and controller.
            // TODO: Select input and controller depending on data.
            input = new PlayerInput();
            controller = new GreedyController();
            // Init the input using the data
            input.Init(data, this);
            // Init de controller using the data
            controller.Init(data, this);

            if (cameraSet != null && cameraSet.Items.Count > 0)
            {
                cameraSet.Items[0].FollowCharacter(transform);
            }
        }

        private void Update()
        {
            CharacterInputData cid = input.Tick();
            controller.Tick(cid);
        }

        private void OnEnable()
        {
            foreach (CharacterSet set in data.SetsToAdd)
            {
                set.Add(this);
            }
        }

        private void OnDisable()
        {
            foreach (CharacterSet set in data.SetsToAdd)
            {
                set.Remove(this);
            }
        }
    }
}
