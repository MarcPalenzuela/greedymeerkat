﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    public abstract class ScriptableState : ScriptableObject
    {
        [System.Serializable]
        protected struct Transition
        {
            public ScriptableCondition condition;
            public ScriptableState trueState;
            public ScriptableState falseState;
        }

        [System.Serializable]
        protected struct MessageTransition
        {
            public string message;
            public ScriptableState state;
            public ScriptableCondition extraCondition;
        }

        [SerializeField]
        protected Transition[] transitions;
        [SerializeField]
        protected MessageTransition[] messageTransitions;

        public void Tick(CharacterController controller, CharacterInputData inputData)
        {
            OnTick(controller, inputData);
            CheckDecisions(controller, inputData);
        }

        private void CheckDecisions(CharacterController controller, CharacterInputData inputData)
        {
            foreach (Transition t in transitions)
            {
                if (t.condition.CheckCondition(controller, inputData))
                {
                    if (t.trueState != null)
                    {
                        controller.ChangeState(t.trueState);
                        break;
                    }
                }
                else
                {
                    if (t.falseState != null)
                    {
                        controller.ChangeState(t.falseState);
                        break;
                    }
                }
            }
        }

        public void Message(CharacterController controller, string message)
        {
            foreach (MessageTransition mt in messageTransitions)
            {
                if (mt.message == message && (!mt.extraCondition || mt.extraCondition.CheckCondition(controller, new CharacterInputData())))
                {
                    controller.ChangeState(mt.state);
                    break;
                }
            }
        }

        public abstract void OnEnterState(CharacterController controller);
        public abstract void OnExitState(CharacterController controller);
        public abstract void OnTick(CharacterController controller, CharacterInputData inputData);

    }
}