﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    [CreateAssetMenu(menuName = "States/Movement State")]
    public class MovementState : ScriptableState
    {
        private UnityEngine.CharacterController chController;
        private float horizontalInput;
        private float verticalInput;
        private Vector3 oldLookDir;
        private Vector3 movement;
        private Vector3 lookDir;
        private Vector3 smoothDir;
        private float turnSpeed = 5f;

        public override void OnEnterState(CharacterController controller)
        {
            // Here we should start an movement animation
            chController = controller.character.GetComponent<UnityEngine.CharacterController>();
        }

        public override void OnExitState(CharacterController controller)
        {
            movement = Vector3.zero;
        }

        public override void OnTick(CharacterController controller, CharacterInputData inputData)
        {
            walk(inputData.inputHorizontal, inputData.inputVertical,controller.data.Walkspeed);
            rotate(controller);            
            chController.Move(movement);
        }

        private void walk(float horizontal, float vertical, float walkSpeed) {
            movement = Vector3.right * horizontal + Vector3.forward * vertical;
            movement /= Mathf.Sqrt(2f);
            movement *= walkSpeed * Time.deltaTime;
        }

        private void rotate(CharacterController controller) {
            oldLookDir = Vector3.zero;
            lookDir = new Vector3(movement.x,0,movement.z);
            smoothDir = Vector3.Slerp(controller.character.transform.forward, lookDir, turnSpeed * Time.deltaTime);
            controller.character.transform.rotation = Quaternion.LookRotation(smoothDir);
            oldLookDir = smoothDir;
        }
    }
}
 