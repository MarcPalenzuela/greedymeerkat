﻿using UnityEngine;

namespace Greedymeerkat {
    [CreateAssetMenu(menuName = "Conditions/Is Walking Condition")]
    public class IsWalkingCondition : ScriptableCondition {
        public override bool CheckCondition(CharacterController controller,CharacterInputData inputData) {
            return (inputData.inputHorizontal != 0 || inputData.inputVertical != 0);
        }
    }
}
