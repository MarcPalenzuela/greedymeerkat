﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Greedymeerkat {
    [CreateAssetMenu(menuName = "Conditions/Is Jumping")]
    public class IsJumping : ScriptableCondition {

        public override bool CheckCondition(CharacterController controller, CharacterInputData inputData) {
            return inputData.jumpAction && controller.character.GetComponent<UnityEngine.CharacterController>().isGrounded;
        }

    }
}
