﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Greedymeerkat {
    [CreateAssetMenu(menuName = "Conditions/Is Second Jump")]
    public class IsDoubleJump : ScriptableCondition {

        public override bool CheckCondition(CharacterController controller, CharacterInputData inputData) {
            return inputData.jumpAction && !controller.character.GetComponent<UnityEngine.CharacterController>().isGrounded && !controller.MadeSecondJump;
        }

    }
}

