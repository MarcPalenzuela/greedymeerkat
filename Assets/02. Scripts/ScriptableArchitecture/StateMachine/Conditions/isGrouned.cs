﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Greedymeerkat {
    [CreateAssetMenu(menuName = "Conditions/Is Grounded")]
    public class isGrouned : ScriptableCondition {
        public override bool CheckCondition(CharacterController controller, CharacterInputData inputData) {
            return controller.character.GetComponent<UnityEngine.CharacterController>().isGrounded;
        }
    }
}
