﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Greedymeerkat {
    [CreateAssetMenu(menuName = "Conditions/Is Falling")]
    public class IsFalling : ScriptableCondition {

        public override bool CheckCondition(CharacterController controller, CharacterInputData inputData) {
            return controller.CurVerticalSpeed <= 0f && !controller.character.GetComponent<UnityEngine.CharacterController>().isGrounded;
        }
    }
}
