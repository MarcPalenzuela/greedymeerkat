﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    [System.Serializable]
    public abstract class ScriptableCondition : ScriptableObject
    {
        public abstract bool CheckCondition(CharacterController controller, CharacterInputData inputData);
    }
}