﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    [CreateAssetMenu(menuName = "States/Idle State")]
    public class IdleState : ScriptableState
    {
        public override void OnEnterState(CharacterController controller)
        {
            // Here we should start an idle animation
            controller.MadeSecondJump = false;
        }

        public override void OnExitState(CharacterController controller)
        {
        }

        public override void OnTick(CharacterController controller, CharacterInputData inputData)
        {
        }
    }
}