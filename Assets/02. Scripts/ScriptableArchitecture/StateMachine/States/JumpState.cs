﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat {

    [CreateAssetMenu(menuName = "States/Jump State")]
    public class JumpState : ScriptableState {

        private float jumpSpeed = 0f;
        private UnityEngine.CharacterController chController;
        [SerializeField]
        private bool isSecondJump = false;

        public override void OnEnterState(CharacterController controller) {
            chController = controller.character.GetComponent<UnityEngine.CharacterController>();
            controller.MadeSecondJump = isSecondJump;
            jumpSpeed = controller.data.JumpSpeed;

        }

        public override void OnExitState(CharacterController controller) {

        }

        public override void OnTick(CharacterController controller, CharacterInputData inputData) {
            if(jumpSpeed > 0f) {
                chController.Move(Vector3.up * jumpSpeed * Time.deltaTime);
                jumpSpeed = jumpSpeed - (controller.data.FallSpeed * Time.deltaTime);
                controller.CurVerticalSpeed = jumpSpeed;
            }
        }
    }
}

