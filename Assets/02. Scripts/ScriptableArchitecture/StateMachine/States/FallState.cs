﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Greedymeerkat {

    [CreateAssetMenu(menuName = "States/Fall State")]
    public class FallState : ScriptableState {

        private float jumpSpeed = 0f;
        private UnityEngine.CharacterController chController;

        public override void OnEnterState(CharacterController controller) {
            chController = controller.character.GetComponent<UnityEngine.CharacterController>();
            jumpSpeed = controller.CurVerticalSpeed;
        }

        public override void OnExitState(CharacterController controller) {

        }

        public override void OnTick(CharacterController controller, CharacterInputData inputData) {
            chController.Move(Vector3.up * jumpSpeed * Time.deltaTime);
            jumpSpeed = jumpSpeed - (controller.data.FallSpeed * Time.deltaTime);
        }
    }
}
