﻿using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    [CreateAssetMenu(menuName = "Sets/Character set")]
    public class CharacterSet : RuntimeSet<Character>
    {
    }
}