﻿using System.Collections.Generic;
using UnityEngine;

namespace Greedymeerkat
{
    [CreateAssetMenu(menuName = "Sets/CharacterCamera set")]
    public class CharacterCameraSet : RuntimeSet<CharacterCamera>
    {
    }
}